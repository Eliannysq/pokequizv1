(function() {
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

angular.module('starter', ['ionic','PokeModule','pascalprecht.translate'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    AppRate.preferences.usesUntilPrompt = 1;
    AppRate.promptForRating(true);

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'js/app/menu/menu.html',
    controller: 'PokeController'
    })

  .state('app.inicio', {
      url: '/inicio',
      views:{
        'content': {
          templateUrl: 'js/app/main/inicio.html'
        }
      }
   })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/inicio');
})

.run(function($ionicPlatform, $translate) {
    $ionicPlatform.ready(function() {
        // The $ionicPlatform.ready event is called when Cordova's deviceready event fires
        if (typeof navigator.globalization !== "undefined") {
            navigator.globalization.getPreferredLanguage(function(language) {
                console.log(language.value)
                $translate.use((language.value).split("-")[0]).then(function(data) {
                    console.log("SUCCESS -> " + data);
                }, function(error) {
                    console.log("ERROR -> " + error);
                });
            }, null);
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $translateProvider) {
    $translateProvider.translations('en', {
        menu: "Menu",
        inicio: "Home",
        quiz: "Quiz",
        otras_app: "Other applications",
        ini_bienv: "Welcome!",
        ini_text: "We invite you to be our PokeQuiz, with simple questions that will help you to know which pokemon you are.",
        titulo1: "Answer the following questions",
        pregunta1: "What is your name?",
        pregunta2: "What is your date of birth?",
        pregunta3: "What is your favorite Color?",
        nombre: "Name",
        dia: "Day",
        mes: "Month",
        message_error: "Insert only lyrics.",
        message_error2: "All fields are required!",
        titulo2: "Result PokeQuiz",
        tit_name: "You are a",
        descripcion: "Description",
        altura: "Height",
        especie: "Species",
        peso: "Weight",
        habilidad: "skill",
        tipo: "Type",
        grupo: "Egg Group",
        punto_base: "Basis points",
        PS: "PS",
        ataque: "Attack",
        defensa: "Defense",
        ataq_esp: "Attack Special",
        def_esp: "Def. Special",
        velocidad: "Speed"

    });
    $translateProvider.translations('es', {

        menu:"Menu",
        inicio: "Inicio",
        quiz: "Quiz",
        otras_app:"Otras Aplicaciones",
        ini_bienv: "Bienvenidos!",
        ini_text: "Te invitamos a realizar nuestro PokeQuiz, con preguntas sencillas que te ayudaran a saber que pokemon eres.",
        titulo1: "Responde las siguientes Preguntas",
        pregunta1: "¿Cuál es tu Nombre?",
        pregunta2: "¿Cuál es tu fecha de nacimiento?",
        pregunta3: "¿Cuál es tu Color Favorito?",
        nombre: "Nombre",
        dia: "Dia",
        mes: "Mes",
        message_error: "Inserte solo Letras.",
        message_error2: "Todos los Campos son requeridos!",
        titulo2: "Resultado PokeQuiz",
        tit_name: "Eres un",
        descripcion: "Descripción",
        altura: "Altura",
        especie: "Especie",
        peso: "Peso",
        habilidad: "Habilidad",
        tipo: "Tipo",
        grupo: "Grupo de Huevo",
        punto_base: "Puntos de Base",
        PS: "PS",
        ataque: "Ataque",
        defensa: "Defensa",
        ataq_esp: "Ataq. Esp.",
        def_esp: "Def. Esp.",
        velocidad: "Velocidad"

    });

    $translateProvider.useSanitizeValueStrategy('escape');

    $translateProvider.preferredLanguage("es");
    $translateProvider.fallbackLanguage("es");
});

})();
