angular.module('PokeModel', [])

.factory('Pokemon', function(){
    
    function Pokemon(name, nro, types, height, weight, abilities, img, species, egg_groups, habitat, hp, attack, defense, sp_atk, sp_def, speed){
        this.id = id;
        this.name = name;
        this.descrip = descrip;
        this.species = species;
        this.type = [type];
        this.height = height;
        this.weight = weight;
        this.abilities = [abilities];
        this.egg_groups = [egg_groups];
        this.hp = hp;
        this.attack = attack;
        this.defense = defense;
        this.sp_atk = sp_atk;
        this.sp_def = sp_def;
        this.speed = speed;

    }
    
    Pokemon.build = function(data){
        if(!data || (!data.name))
            return null;
        
        //check if properties start by uppercase
        if('name' in data){
            var lowerCaseStartingData = {};
            var key, keys = Object.keys(data);
            for(var i=0; i<keys.length; i++){
                key = keys[i].charAt(0).toLowerCase() + keys[i].slice(1);
                lowerCaseStartingData[key] = data[keys[i]];
            }
            data = lowerCaseStartingData;
        }
        
        //build film, using lowercase starting properties
        return new Pokemon(data.name, data.nro, data.types, data.height, data.weight, data.abilities, data.img, data.species, data.egg_groups, data.habitat, data.hp, data.attack, data.defense, data.sp_atk, data.sp_def, data.speed);
    }
    
    Pokemon.prototype.toJson = function(){
        return angular.toJson(this);
    }
    
    Pokemon.fromJsonBunch = function(data){
        if(angular.isArray(data)){
            return data.map(Pokemon.build).filter(Boolean);
        }
        return Pokemon.build(data);
    }

    return Pokemon;
})