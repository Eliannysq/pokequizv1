(function() {

    angular.module('PokeapiPokeModule', [])

    .factory('pokeService', ['$http', '$q', '$translate', function($http, $q,$translate){

        var pokeservice = [];
        var currentLang = ($translate.proposedLanguage() || $translate.use());

        function _all(){

            var deferred = $q.defer();

            if(currentLang==='es'){
                $http.get('js/pokemones_Es.json')
                .success(function (data) {
                deferred.resolve(data);
              });
            }else{
                $http.get('js/pokemones_En.json')
                .success(function (data) {
                deferred.resolve(data);
              });
            }
            return deferred.promise;
        };

        pokeservice.getPokemon = function(id){
            var deferred = $q.defer();
            _all().then(function(data) {
                for(var j= 0; j<data.length; j++){
                    if(data[j].id === id){
                        pokeservice=data[j];
                    }
                }
                deferred.resolve(pokeservice);
            },
            function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };


        return pokeservice;
    }])

})();