(function () {

  angular.module('Filters', [])

 //filtro para cambiar caracteres especiales
    .filter('configurar', function () {
      return function (input) {
          if (!input) return "";

          input = input
                  .replace('♀', 'f')
                  .replace('♂', 'm')
                  .replace(/\W+/g, "");
          return input.toLowerCase();
      };
    })


    //filtro para poner la primera letra en mayúscula
    .filter("mayuscula", function(){
      return function(text) {
          if(text != null){
              return text.substring(0,1).toUpperCase()+text.substring(1);
          }
      };
    })

    .filter("mayus", function(){
    return function(text) {
    if(text != null){
            return text.toUpperCase();
          }
      };
})

})();
